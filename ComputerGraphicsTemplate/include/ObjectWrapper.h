#pragma once

#include "utilities/Model.h"
#include "interfaces/IRaw3DObject.h"
#include "Material.h"
#include "MaterialPresets.h"

class ObjectWrapper
{
public:
	ObjectWrapper(IRaw3DObject &rawObject, const Material &material = MAT_DEFAULT);

	void draw();
	void translate(const glm::vec3 &position);
	void rotate(const float &degree, const glm::vec3 &axes);
	void scale(const glm::vec3 &scale);
	void reset();
	void setActive(bool isActive);
	glm::mat4 getModelMatrix();
	glm::vec4 getPosition();
	const Material &getMaterial() const;
	void setMaterial(const Material &material);

protected:
	Model _model;
	IRaw3DObject &_rawObject;
	bool _isActive;
	Material _material;
};
