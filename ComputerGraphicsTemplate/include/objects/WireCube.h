#pragma once

#include <GL/glew.h>
#include "interfaces/IRaw3DObject.h"

class WireCube : public IRaw3DObject
{
public:
	WireCube();
	~WireCube() = default;
	void draw() final;

private:
	void _setup();

	GLuint _vaoHandle;
	GLuint _vboCubeVertices;
	GLuint _vboCubeColors;
};
