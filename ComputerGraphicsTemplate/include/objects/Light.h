#pragma once

#include <GL/glew.h>
#include <glm/vec3.hpp>
#include "interfaces/IRaw3DObject.h"
#include "Material.h"

class Light : public IRaw3DObject
{
public:
	Light();
	~Light() = default;
	void draw() final;

	static Material createMaterialFromIntensity(const glm::vec3 &intensity);
	static glm::vec3 getIntensityFromMaterial(const Material &material);

private:
	void _setup();

	GLuint _vaoHandle;
	GLuint _vboLightVertices;
	GLuint _vboLightColors;
};
