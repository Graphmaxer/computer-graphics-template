#pragma once

#include <GL/glew.h>
#include "interfaces/IRaw3DObject.h"

class LineSegment : public IRaw3DObject
{
public:
	LineSegment();
	~LineSegment() = default;
	void draw() final;

private:
	void _setup();

	GLuint _vaoHandle;
	GLuint _vboLineVertices;
	GLuint _vboLineColors;
};
