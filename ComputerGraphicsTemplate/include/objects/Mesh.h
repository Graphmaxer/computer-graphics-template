#pragma once

#include "GL/glew.h"
#include <GLFW/glfw3.h>

#include <vector>

#include "assimp\scene.h"
#include "assimp\mesh.h"
#include "utilities/ShaderProgram.h"

#include "interfaces/IRaw3DObject.h"

class Mesh : public IRaw3DObject
{
public:
	struct MeshEntry {
		const enum BUFFERS {
			VERTEX_BUFFER, TEXCOORD_BUFFER, NORMAL_BUFFER, INDEX_BUFFER
		};
		GLuint vao;
		GLuint vbo[4];

		unsigned int elementCount;
		aiColor3D dcolor;
		aiColor3D acolor;
		aiColor3D scolor;
		float shininessStrength;
		MeshEntry(aiMesh *mesh, const aiScene* scene, Mesh * m);
		~MeshEntry();
		Mesh * parent;
		void render();
	};

public:
	Mesh(const char *filename, ShaderProgram * sh, bool enableMaterial = false);
	~Mesh(void);

	std::vector<MeshEntry*> meshEntries;

	ShaderProgram * shader;
	void draw();

private:
	bool _isMaterialEnabled;
};
