#pragma once

#include <GL/glew.h>
#include "interfaces/IRaw3DObject.h"

class CheckeredFloor : public IRaw3DObject
{
public:
	CheckeredFloor(int size, int nSquares);
	~CheckeredFloor() = default;
	void draw() final;

private:
	void _setup();

	int _size;
	int _nSquares;
	GLuint _vaoHandle;
	GLuint _vboTileVertices;
	GLuint _vboTileColors;
};
