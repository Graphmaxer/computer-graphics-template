#pragma once

#include <GL/glew.h>
#include "interfaces/IRaw3DObject.h"

class Plane : public IRaw3DObject
{
public:
	Plane(int size, int nSquares);
	~Plane() = default;
	void draw() final;

private:
	void _setup();

	int _size;
	int _nSquares;
	GLuint _vaoHandle;
	GLuint _vboTileVertices;
	GLuint _vboTileNormals;
};
