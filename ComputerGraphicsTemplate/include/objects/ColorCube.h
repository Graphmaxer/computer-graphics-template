#pragma once

#include <GL/glew.h>
#include "interfaces/IRaw3DObject.h"

class ColorCube : public IRaw3DObject
{
public:
	ColorCube();
	~ColorCube() = default;
	void draw() final;

private:
	void _setup();

	GLuint _vaoHandle;
	GLuint _vboCubeVertices;
	GLuint _vboCubeColors;
	GLuint _iboCubeElements;
};
