#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "lib/imgui/imgui.h"
#include "lib/imgui/imgui_impl_glfw.h"
#include "lib/imgui/imgui_impl_opengl3.h"
#include "GlWindow.h"
#include "InputHandler.h"

class GlApp
{
public:
	GlApp(int width, int height);
	~GlApp();
	void run();

private:
	void _initialize();
	void _initGlfw();
	void _initImGui();
	void _initGlew();
	void _initInputHandler();

	int _width;
	int _height;
	GlWindow *_myWindow;
	GLFWwindow *_glfwWindow;
	InputHandler *_inputHandler;

	const static char *GLSL_VERSION;
};
