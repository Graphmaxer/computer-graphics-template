#pragma once

#include "ObjectWrapper.h"

class LightWrapper : public ObjectWrapper {
public:
	LightWrapper(
		IRaw3DObject &rawObject,
		const Material &material = MAT_DEFAULT,
		float cutoff = 0,
		float innerCutoff = 0,
		float exponent = 0,
		const glm::vec3 &lookAtPos = {0, 0, 0}
	);

	float getCutoff() const;
	float getInnerCutoff() const;
	float getExponent() const;
	glm::vec4 getLookAtPos() const;

private:
	float _cutoff;
	float _innerCutoff;
	float _exponent;
	glm::vec4 _lookAtPos;
};
