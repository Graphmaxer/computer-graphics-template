#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/vec3.hpp>
#include <memory>
#include <vector>
#include "utilities/Viewer.h"
#include "interfaces/IScene.h"

class GlWindow
{
public:
	GlWindow(int width, int height);
	void setSize(int width, int height);
	void draw();
	Viewer &getViewer();

private:
	int _width;
	int _height;
	ImVec4 _clearColor;
	std::vector<std::unique_ptr<IScene>> _scenes;
	int _currentSceneIdx;

	void _drawSceneManagerUi();
};
