#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "GlWindow.h"

class InputHandler
{
public:
	InputHandler(GlWindow &window);
	static void windowSizeCallback(GLFWwindow *glfwWindow, int width, int height);
	static void keyCallback(GLFWwindow *glfwWindow, int key, int scancode, int action, int mods);
	static void cursePosCallback(GLFWwindow *glfwWindow, double xpos, double ypos);
	static void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods);
	void handleWindowSize(GLFWwindow *glfwWindow, int width, int height);
	void handleKey(GLFWwindow *glfwWindow, int key, int scancode, int action, int mods);
	void handleCursePos(GLFWwindow *glfwWindow, double xpos, double ypos);
	void handleMouseButton(GLFWwindow *window, int button, int action, int mods);
	void handleMouseDragging(float width, float height);

private:
	static InputHandler *_getInputHandlerFromGlfwWindow(GLFWwindow *glfwWindow);

	GlWindow &_window;
	bool _lbuttonDown;
	bool _rbuttonDown;
	bool _mbuttonDown;
	bool _zoomInDown;
	bool _zoomOutDown;
	double _mouseX;
	double _mouseY;
	double _lastMouseX;
	double _lastMouseY;
};
