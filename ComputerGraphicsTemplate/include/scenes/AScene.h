#pragma once

#include <vector>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include "utilities/ShaderProgram.h"
#include "utilities/Viewer.h"
#include "interfaces/IScene.h"
#include "ObjectWrapper.h"
#include "LightWrapper.h"

class AScene : public IScene
{
public:
	AScene(int width, int height, const std::string &name);
	virtual ~AScene() = default;

	void init() final;
	void render() final;
	void setSize(int width, int height) final;
	Viewer &getViewer() final;
	const std::string &getName() const final;

protected:
	int _width;
	int _height;
	Viewer _viewer;
	ShaderProgram _shaderProgramSimple;
	ShaderProgram _shaderProgramPhong;
	ShaderProgram _shaderProgramPhongSpotlight;
	std::vector<ObjectWrapper> _objects;
	std::vector<ObjectWrapper> _objectsInLight;
	std::vector<ObjectWrapper> _objectsInSpotlight;
	std::vector<LightWrapper> _lights;
	std::string _name;

	const static glm::vec3 DEFAULT_VIEW_POINT;
	const static glm::vec3 DEFAULT_VIEW_CENTER;
	const static glm::vec3 DEFAULT_UP_VECTOR;

	void _initShader();
	virtual void _initScene() = 0;
	virtual void _setupImGui() = 0;
	virtual void _setupScene() = 0;
	virtual glm::mat4 _calcMvp(glm::mat4 model);
	void _resetScene();
	void _updateLightUniforms();
};
