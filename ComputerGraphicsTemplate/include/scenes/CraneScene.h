#pragma once

#include <memory>
#include "AScene.h"
#include "interfaces/IRaw3DObject.h"

class CraneScene : public AScene
{
public:
	CraneScene(int width, int height);
	~CraneScene() = default;

private:
	std::unique_ptr<IRaw3DObject> _colorCube;
	std::unique_ptr<IRaw3DObject> _checkeredFloor;
	std::unique_ptr<IRaw3DObject> _wireCube;
	std::unique_ptr<IRaw3DObject> _lineSegment;

	const static int CRANE_HEIGHT;
	const static int CRANE_LENGHT;

	enum CameraType {
		DEFAULT,
		BOX,
	};

	float _boomRotation;
	float _boxHorizontal;
	float _boxVertical;
	int _cameraType;

	glm::mat4 _calcMvp(glm::mat4 model) final;
	void _initScene() final;
	void _setupImGui() final;
	void _setupScene() final;
};
