#pragma once

#include <memory>
#include "AScene.h"
#include "interfaces/IRaw3DObject.h"

class DragonScene : public AScene
{
public:
	DragonScene(int width, int height);
	~DragonScene() = default;

private:
	std::unique_ptr<IRaw3DObject> _checkeredFloor;
	std::unique_ptr<IRaw3DObject> _mesh;
	std::unique_ptr<IRaw3DObject> _light;
	float _lightRingY;
	float _lightRingRadius;
	int _nbLight;

	const static std::vector<glm::vec3> LIGHT_INTENSITIES;

	void _initScene() final;
	void _setupImGui() final;
	void _setupScene() final;

	void _refreshLights();
};
