#pragma once

#include <memory>
#include "AScene.h"
#include "interfaces/IRaw3DObject.h"

class ThreeCubesScene : public AScene
{
public:
	ThreeCubesScene(int width, int height);
	~ThreeCubesScene() = default;

private:
	std::unique_ptr<IRaw3DObject> _colorCube;
	std::unique_ptr<IRaw3DObject> _checkeredFloor;

	float _cubesRotation;
	float _cubesPositionX;
	float _cubesPositionY;

	void _initScene() final;
	void _setupImGui() final;
	void _setupScene() final;
};
