#pragma once

#include <memory>
#include "AScene.h"
#include "interfaces/IRaw3DObject.h"

class SpotlightScene : public AScene
{
public:
	SpotlightScene(int width, int height);
	~SpotlightScene() = default;

private:
	std::unique_ptr<IRaw3DObject> _plane;
	std::unique_ptr<IRaw3DObject> _teapot;
	std::unique_ptr<IRaw3DObject> _light;

	float _lightRotation;

	void _initScene() final;
	void _setupImGui() final;
	void _setupScene() final;
};
