#pragma once

#include <memory>
#include "AScene.h"
#include "interfaces/IRaw3DObject.h"

class TeapotSphereScene : public AScene
{
public:
	TeapotSphereScene(int width, int height);
	~TeapotSphereScene() = default;

private:
	std::unique_ptr<IRaw3DObject> _checkeredFloor;
	std::unique_ptr<IRaw3DObject> _teapot;
	std::unique_ptr<IRaw3DObject> _sphere;
	std::unique_ptr<IRaw3DObject> _light;

	glm::vec3 _lightPosition;

	void _initScene() final;
	void _setupImGui() final;
	void _setupScene() final;
};
