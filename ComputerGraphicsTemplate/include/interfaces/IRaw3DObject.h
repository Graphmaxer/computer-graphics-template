#pragma once

class IRaw3DObject
{
public:
	IRaw3DObject() {};
	virtual ~IRaw3DObject() = default;

	virtual void draw() = 0;
};
