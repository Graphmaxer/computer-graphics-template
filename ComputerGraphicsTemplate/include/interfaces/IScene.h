#pragma once

#include "utilities/Viewer.h"

class IScene {
public:
	IScene() {};
	virtual ~IScene() = default;

	virtual void init() = 0;
	virtual void render() = 0;
	virtual void setSize(int width, int height) = 0;
	virtual Viewer &getViewer() = 0;
	virtual const std::string &getName() const = 0;
};
