#include "lib/imgui/imgui.h"
#include "scenes/ThreeCubesScene.h"
#include "objects/ColorCube.h"
#include "objects/CheckeredFloor.h"

ThreeCubesScene::ThreeCubesScene(int width, int height) :
	AScene(width, height, "Three Cubes"),
	_colorCube(std::make_unique<ColorCube>()),
	_checkeredFloor(std::make_unique<CheckeredFloor>(100, 16)),
	_cubesRotation(0.0f),
	_cubesPositionX(0.0f),
	_cubesPositionY(0.0f)
{
}

void ThreeCubesScene::_initScene()
{
	_objects.push_back(ObjectWrapper(*_checkeredFloor.get()));
	_objects.push_back(ObjectWrapper(*_colorCube.get()));
	_objects.push_back(ObjectWrapper(*_colorCube.get()));
	_objects.push_back(ObjectWrapper(*_colorCube.get()));
}

void ThreeCubesScene::_setupImGui()
{
	if (ImGui::Begin("Cubes control")) {
		ImGui::SliderFloat("Cubes Rotation", &_cubesRotation, 0, 360);
		ImGui::SliderFloat("Cubes position X", &_cubesPositionX, -20, 20);
		ImGui::SliderFloat("Cubes position Y", &_cubesPositionY, -20, 20);
	}
	ImGui::End();
}

void ThreeCubesScene::_setupScene()
{
	_objects[1].translate({ _cubesPositionX, 0, _cubesPositionY });
	_objects[2].translate({ _cubesPositionX, 0, _cubesPositionY });
	_objects[3].translate({ _cubesPositionX, 0, _cubesPositionY });
	_objects[1].rotate(_cubesRotation, { 0, 1, 0 });
	_objects[2].rotate(_cubesRotation, { 0, 1, 0 });
	_objects[3].rotate(_cubesRotation, { 0, 1, 0 });
	_objects[1].translate({ 0, 1, 0 });
	_objects[2].translate({ 2, 1, 0 });
	_objects[3].translate({ 4, 1, 0 });
}
