#include "lib/imgui/imgui.h"
#include "scenes/CraneScene.h"
#include "objects/ColorCube.h"
#include "objects/CheckeredFloor.h"
#include "objects/WireCube.h"
#include "objects/LineSegment.h"

const int CraneScene::CRANE_HEIGHT = 8;
const int CraneScene::CRANE_LENGHT = 7;

CraneScene::CraneScene(int width, int height) :
	AScene(width, height, "Crane"),
	_colorCube(std::make_unique<ColorCube>()),
	_checkeredFloor(std::make_unique<CheckeredFloor>(100, 16)),
	_wireCube(std::make_unique<WireCube>()),
	_lineSegment(std::make_unique<LineSegment>()),
	_boomRotation(0.0f),
	_boxHorizontal(0.0f),
	_boxVertical(0.0f),
	_cameraType(DEFAULT)
{
}

glm::mat4 CraneScene::_calcMvp(glm::mat4 model)
{
	glm::vec3 eye = _viewer.getViewPoint();
	glm::vec3 look = _viewer.getViewCenter();
	glm::vec3 up = _viewer.getUpVector();

	if (_cameraType == BOX) {
		eye = _objects[CRANE_HEIGHT + CRANE_LENGHT + 2].getModelMatrix() * glm::vec4(0, 0, 0, 1);
		look = _objects[CRANE_HEIGHT + CRANE_LENGHT + 2].getModelMatrix() * glm::vec4(1, 0, 0, 1);
	}

	glm::mat4 view = glm::lookAt(eye, look, up);
	glm::mat4 projection = glm::perspective(45.0f, 1.0f * _width / _height, 0.1f, 500.0f);
	return projection * view * model;
}

void CraneScene::_initScene()
{
	// Floor
	_objects.push_back(ObjectWrapper(*_checkeredFloor.get()));
	// Crane
	for (int i = 0; i < CRANE_HEIGHT - 1; i++) {
		_objects.push_back(ObjectWrapper(*_wireCube.get()));
	}
	for (int i = 0; i < CRANE_LENGHT + 1; i++) {
		_objects.push_back(ObjectWrapper(*_wireCube.get()));
	}
	// Crane cable
	_objects.push_back(ObjectWrapper(*_lineSegment.get()));
	// Cube on crane
	_objects.push_back(ObjectWrapper(*_colorCube.get()));
	// Static cube
	_objects.push_back(ObjectWrapper(*_colorCube.get()));
}

void CraneScene::_setupImGui()
{
	if (ImGui::Begin("Control Window")) {
		ImGui::Text("Crane Control");
		ImGui::SliderFloat("Boom Rotation", &_boomRotation, -180.0f, 180.0f);
		ImGui::SliderFloat("Box Horizontal", &_boxHorizontal, 0.0f, 1.0f, "ratio = %.3f");
		ImGui::SliderFloat("Box Vertical", &_boxVertical, 0.0f, 1.0f, "ratio = %.3f");
		ImGui::RadioButton("Default", &_cameraType, DEFAULT);
		ImGui::SameLine();
		ImGui::RadioButton("Box", &_cameraType, BOX);
	}
	ImGui::End();
}

void CraneScene::_setupScene()
{
	if (_cameraType == BOX)
		_viewer.setIsActive(false);
	else
		_viewer.setIsActive(true);
	// Crane
	for (int i = 0; i < CRANE_LENGHT + 1; i++) {
		_objects[i + CRANE_HEIGHT].rotate(_boomRotation, { 0, 1, 0 });
	}
	for (int i = 0; i < CRANE_HEIGHT - 1; i++) {
		_objects[i + 1].translate({ 0, 1 + i * 2, 0 });
	}
	for (int i = 0; i < CRANE_LENGHT + 1; i++) {
		_objects[i + CRANE_HEIGHT].translate({ -2 + i * 2, 1 + (CRANE_HEIGHT - 1) * 2, 0 });
	}
	// Crane cable
	_objects[CRANE_HEIGHT + CRANE_LENGHT + 1].rotate(_boomRotation, { 0, 1, 0 });
	_objects[CRANE_HEIGHT + CRANE_LENGHT + 1].translate({
		2 + _boxHorizontal * (CRANE_LENGHT - 2) * 2,
		(CRANE_HEIGHT - 1 ) * 2,
		0
	});
	_objects[CRANE_HEIGHT + CRANE_LENGHT + 1].scale({ 1, (1 - _boxVertical) * (CRANE_HEIGHT - 2), 1 });
	_objects[CRANE_HEIGHT + CRANE_LENGHT + 1].translate({ 0, -1, 0 });
	// Cube on crane
	if (_cameraType == BOX)
		_objects[CRANE_HEIGHT + CRANE_LENGHT + 2].setActive(false);
	else
		_objects[CRANE_HEIGHT + CRANE_LENGHT + 2].setActive(true);
	_objects[CRANE_HEIGHT + CRANE_LENGHT + 2].rotate(_boomRotation, { 0, 1, 0 });
	_objects[CRANE_HEIGHT + CRANE_LENGHT + 2].translate({
		2 + _boxHorizontal * (CRANE_LENGHT - 2) * 2,
		1 + _boxVertical * (CRANE_HEIGHT - 2) * 2,
		0
		});
	// Static cube
	_objects[CRANE_HEIGHT + CRANE_LENGHT + 3].translate({ 9, 1, 9 });
}
