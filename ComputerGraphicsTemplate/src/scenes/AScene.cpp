#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>
#include "scenes/AScene.h"
#include "objects/Light.h"

const glm::vec3 AScene::DEFAULT_VIEW_POINT = { 5, 5, 5 };
const glm::vec3 AScene::DEFAULT_VIEW_CENTER = { 0, 0, 0 };
const glm::vec3 AScene::DEFAULT_UP_VECTOR = { 0, 1, 0 };

AScene::AScene(int width, int height, const std::string &name) :
	_width(width),
	_height(height),
	_viewer(DEFAULT_VIEW_POINT, DEFAULT_VIEW_CENTER, DEFAULT_UP_VECTOR, 45.0f,
		static_cast<float>(width) / static_cast<float>(height)),
	_shaderProgramSimple(),
	_shaderProgramPhong(),
	_shaderProgramPhongSpotlight(),
	_objects(),
	_name(name)
{
}

void AScene::init()
{
	_initScene();
	_initShader();
}

void AScene::_initShader()
{
	_shaderProgramSimple.initFromFiles("shaders/simple.vert", "shaders/simple.frag");
	_shaderProgramSimple.addAttribute("v_position");
	_shaderProgramSimple.addAttribute("v_color");
	_shaderProgramSimple.addUniform("u_mvp");

	_shaderProgramPhong.initFromFiles("shaders/phong2.vert", "shaders/phong2.frag");
	_shaderProgramPhong.addAttribute("v_position");
	_shaderProgramPhong.addAttribute("v_normal");
	_shaderProgramPhong.addUniform("u_mvp");
	_shaderProgramPhong.addUniform("u_modelViewMatrix");
	_shaderProgramPhong.addUniform("u_normalMatrix");
	for (size_t i = 0; i < _lights.size(); i++)
	{
		_shaderProgramPhong.addUniform("u_lights[" + std::to_string(i) + "].position");
		_shaderProgramPhong.addUniform("u_lights[" + std::to_string(i) + "].intensity");
	}
	_shaderProgramPhong.addUniform("u_nbLights");
	_shaderProgramPhong.addUniform("u_Ka");
	_shaderProgramPhong.addUniform("u_Kd");
	_shaderProgramPhong.addUniform("u_Ks");
	_shaderProgramPhong.addUniform("u_shininess");

	_shaderProgramPhongSpotlight.initFromFiles("shaders/phongSpotlight.vert", "shaders/phongSpotlight.frag");
	_shaderProgramPhongSpotlight.addAttribute("v_position");
	_shaderProgramPhongSpotlight.addAttribute("v_normal");
	_shaderProgramPhongSpotlight.addUniform("u_mvp");
	_shaderProgramPhongSpotlight.addUniform("u_modelViewMatrix");
	_shaderProgramPhongSpotlight.addUniform("u_normalMatrix");
	for (size_t i = 0; i < _lights.size(); i++)
	{
		_shaderProgramPhongSpotlight.addUniform("u_lights[" + std::to_string(i) + "].position");
		_shaderProgramPhongSpotlight.addUniform("u_lights[" + std::to_string(i) + "].intensity");
		_shaderProgramPhongSpotlight.addUniform("u_lights[" + std::to_string(i) + "].cutoff");
		_shaderProgramPhongSpotlight.addUniform("u_lights[" + std::to_string(i) + "].innerCutoff");
		_shaderProgramPhongSpotlight.addUniform("u_lights[" + std::to_string(i) + "].exponent");
		_shaderProgramPhongSpotlight.addUniform("u_lights[" + std::to_string(i) + "].direction");
	}
	_shaderProgramPhongSpotlight.addUniform("u_nbLights");
	_shaderProgramPhongSpotlight.addUniform("u_Ka");
	_shaderProgramPhongSpotlight.addUniform("u_Kd");
	_shaderProgramPhongSpotlight.addUniform("u_Ks");
	_shaderProgramPhongSpotlight.addUniform("u_shininess");
}

void AScene::render()
{
	_setupImGui();
	_setupScene();

	_shaderProgramSimple.use();
	for (auto &object : _objects) {
		glm::mat4 mvp = _calcMvp(object.getModelMatrix());
		glUniformMatrix4fv(_shaderProgramSimple.uniform("u_mvp"), 1, GL_FALSE, glm::value_ptr(mvp));
		object.draw();
	}
	_shaderProgramSimple.disable();

	_shaderProgramPhong.use();
	for (auto &object : _objectsInLight) {
		glm::mat4 model = object.getModelMatrix();
		glm::mat4 mvp = _calcMvp(model);
		glm::vec3 eye = _viewer.getViewPoint();
		glm::vec3 look = _viewer.getViewCenter();
		glm::vec3 up = _viewer.getUpVector();

		glm::mat4 view = glm::lookAt(eye, look, up);
		glm::mat4 modelViewMatrix = view * model;
		glm::mat3 normalMatrix = glm::mat3(glm::transpose(glm::inverse(modelViewMatrix)));
		glUniformMatrix4fv(_shaderProgramPhong.uniform("u_mvp"), 1, GL_FALSE, glm::value_ptr(mvp));
		glUniformMatrix4fv(_shaderProgramPhong.uniform("u_modelViewMatrix"), 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
		glUniformMatrix3fv(_shaderProgramPhong.uniform("u_normalMatrix"), 1, GL_FALSE, glm::value_ptr(normalMatrix));

		for (size_t i = 0; i < _lights.size(); i++)
		{
			std::string name = "u_lights[" + std::to_string(i);
			glm::vec4 lightPos = view * _lights[i].getPosition();
			glUniform4fv(_shaderProgramPhong.uniform(name + "].position"), 1, glm::value_ptr(lightPos));
			glUniform3fv(_shaderProgramPhong.uniform(name + "].intensity"), 1, glm::value_ptr(Light::getIntensityFromMaterial(_lights[i].getMaterial())));
		}
		glUniform1i(_shaderProgramPhong.uniform("u_nbLights"), _lights.size());
		const Material &mat = object.getMaterial();
		glUniform3fv(_shaderProgramPhong.uniform("u_Ka"), 1, glm::value_ptr(mat.Ka));
		glUniform3fv(_shaderProgramPhong.uniform("u_Kd"), 1, glm::value_ptr(mat.Kd));
		glUniform3fv(_shaderProgramPhong.uniform("u_Ks"), 1, glm::value_ptr(mat.Ks));
		glUniform1fv(_shaderProgramPhong.uniform("u_shininess"), 1, &mat.shininess);
		object.draw();
	}
	_shaderProgramPhong.disable();

	_shaderProgramPhongSpotlight.use();
	for (auto &object : _objectsInSpotlight) {
		glm::mat4 model = object.getModelMatrix();
		glm::mat4 mvp = _calcMvp(model);
		glm::vec3 eye = _viewer.getViewPoint();
		glm::vec3 look = _viewer.getViewCenter();
		glm::vec3 up = _viewer.getUpVector();

		glm::mat4 view = glm::lookAt(eye, look, up);
		glm::mat4 modelViewMatrix = view * model;
		glm::mat3 normalMatrix = glm::mat3(glm::transpose(glm::inverse(modelViewMatrix)));
		glUniformMatrix4fv(_shaderProgramPhongSpotlight.uniform("u_mvp"), 1, GL_FALSE, glm::value_ptr(mvp));
		glUniformMatrix4fv(_shaderProgramPhongSpotlight.uniform("u_modelViewMatrix"), 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
		glUniformMatrix3fv(_shaderProgramPhongSpotlight.uniform("u_normalMatrix"), 1, GL_FALSE, glm::value_ptr(normalMatrix));

		for (size_t i = 0; i < _lights.size(); i++)
		{
			std::string name = "u_lights[" + std::to_string(i);
			glm::vec4 lightPos = view * _lights[i].getPosition();
			glm::vec3 direction = (view * _lights[i].getLookAtPos()) - lightPos;
			glUniform4fv(_shaderProgramPhongSpotlight.uniform(name + "].position"), 1, glm::value_ptr(lightPos));
			glUniform3fv(_shaderProgramPhongSpotlight.uniform(name + "].intensity"), 1, glm::value_ptr(Light::getIntensityFromMaterial(_lights[i].getMaterial())));
			glUniform1f(_shaderProgramPhongSpotlight.uniform(name + "].cutoff"), glm::radians(_lights[i].getCutoff()));
			glUniform1f(_shaderProgramPhongSpotlight.uniform(name + "].innerCutoff"), glm::radians(_lights[i].getInnerCutoff()));
			glUniform1f(_shaderProgramPhongSpotlight.uniform(name + "].exponent"), _lights[i].getExponent());
			glUniform3fv(_shaderProgramPhongSpotlight.uniform(name + "].direction"), 1, glm::value_ptr(direction));
		}
		glUniform1i(_shaderProgramPhongSpotlight.uniform("u_nbLights"), _lights.size());
		const Material &mat = object.getMaterial();
		glUniform3fv(_shaderProgramPhongSpotlight.uniform("u_Ka"), 1, glm::value_ptr(mat.Ka));
		glUniform3fv(_shaderProgramPhongSpotlight.uniform("u_Kd"), 1, glm::value_ptr(mat.Kd));
		glUniform3fv(_shaderProgramPhongSpotlight.uniform("u_Ks"), 1, glm::value_ptr(mat.Ks));
		glUniform1fv(_shaderProgramPhongSpotlight.uniform("u_shininess"), 1, &mat.shininess);
		object.draw();
	}
	_shaderProgramPhongSpotlight.disable();

	_shaderProgramSimple.use();
	for (auto &light : _lights) {
		glm::mat4 mvp = _calcMvp(light.getModelMatrix());
		glUniformMatrix4fv(_shaderProgramSimple.uniform("u_mvp"), 1, GL_FALSE, glm::value_ptr(mvp));
		light.draw();
	}
	_shaderProgramSimple.disable();

	_resetScene();
}

void AScene::setSize(int width, int height)
{
	_width = width;
	_height = height;
}

Viewer &AScene::getViewer()
{
	return _viewer;
}

const std::string &AScene::getName() const
{
	return _name;
}

glm::mat4 AScene::_calcMvp(glm::mat4 model)
{
	glm::vec3 eye = _viewer.getViewPoint();
	glm::vec3 look = _viewer.getViewCenter();
	glm::vec3 up = _viewer.getUpVector();

	glm::mat4 view = glm::lookAt(eye, look, up);
	glm::mat4 projection = glm::perspective(45.0f, 1.0f * _width / _height, 0.1f, 500.0f);
	return projection * view * model;
}

void AScene::_resetScene()
{
	for (auto &object : _objects) {
		object.reset();
	}
	for (auto &object : _objectsInLight) {
		object.reset();
	}
	for (auto &object : _objectsInSpotlight) {
		object.reset();
	}
	for (auto &light : _lights) {
		light.reset();
	}
}

void AScene::_updateLightUniforms()
{
	for (size_t i = 0; i < _lights.size(); i++)
	{
		_shaderProgramPhong.addUniform("u_lights[" + std::to_string(i) + "].position");
		_shaderProgramPhong.addUniform("u_lights[" + std::to_string(i) + "].intensity");
		_shaderProgramPhongSpotlight.addUniform("u_lights[" + std::to_string(i) + "].position");
		_shaderProgramPhongSpotlight.addUniform("u_lights[" + std::to_string(i) + "].intensity");
		_shaderProgramPhongSpotlight.addUniform("u_lights[" + std::to_string(i) + "].cutoff");
		_shaderProgramPhongSpotlight.addUniform("u_lights[" + std::to_string(i) + "].exponent");
		_shaderProgramPhongSpotlight.addUniform("u_lights[" + std::to_string(i) + "].direction");
		_shaderProgramPhongSpotlight.addUniform("u_lights[" + std::to_string(i) + "].innerCutoff");
	}
}
