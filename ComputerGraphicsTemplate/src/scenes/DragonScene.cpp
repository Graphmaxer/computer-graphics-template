#include "lib/imgui/imgui.h"
#include "scenes/DragonScene.h"
#include "objects/CheckeredFloor.h"
#include "objects/Mesh.h"
#include "objects/Light.h"

const std::vector<glm::vec3> DragonScene::LIGHT_INTENSITIES = {
	{ 0.0f, 0.8f, 0.8f },
	{ 0.0f, 0.0f, 0.8f },
	{ 0.8f, 0.0f, 0.0f },
	{ 0.0f, 0.8f, 0.0f },
	{ 0.8f, 0.8f, 0.8f }
};

DragonScene::DragonScene(int width, int height) :
	AScene(width, height, "Dragon"),
	_checkeredFloor(std::make_unique<CheckeredFloor>(100, 16)),
	_mesh(std::make_unique<Mesh>("dragon.obj", &_shaderProgramPhong)),
	_light(std::make_unique<Light>()),
	_lightRingY(10.0f),
	_lightRingRadius(10.0f),
	_nbLight(LIGHT_INTENSITIES.size())
{
}

void DragonScene::_initScene()
{
	Material dragonMat = {
		{ 0.1f, 0.1f, 0.1f },
		{ 0.4f, 0.4f, 0.4f },
		{ 0.9f, 0.9f, 0.9f },
		180.0f
	};
	_objects.push_back(ObjectWrapper(*_checkeredFloor.get()));
	_objectsInLight.push_back(ObjectWrapper(*_mesh.get(), dragonMat));
	for (auto &intensity : LIGHT_INTENSITIES) {
		_lights.push_back(LightWrapper(*_light.get(), Light::createMaterialFromIntensity(intensity)));
	}
}

void DragonScene::_setupImGui()
{
	if (ImGui::Begin("Light Ring control")) {
		if (ImGui::SliderInt("Nb lights", &_nbLight, 0, LIGHT_INTENSITIES.size())) {
			_refreshLights();
		}
		ImGui::SliderFloat("Y level", &_lightRingY, 0.0f, 20.0f);
		ImGui::SliderFloat("Radius", &_lightRingRadius, 5.0f, 20.0f);
	}
	ImGui::End();
}

void DragonScene::_setupScene()
{
	float angle = 0;
	for (auto &light : _lights)
	{
		float angleRad = glm::radians(angle);
		light.translate({ _lightRingRadius * glm::cos(angleRad), _lightRingY, _lightRingRadius * glm::sin(angleRad) });
		angle += 360.0f / _lights.size();
	}
	_objectsInLight[0].translate({ 0, 4, 0 });
	_objectsInLight[0].scale({ 10, 10, 10 });
}

void DragonScene::_refreshLights()
{
	_lights.clear();
	for (int i = 0; i < _nbLight; i++) {
		_lights.push_back(LightWrapper(*_light.get(), Light::createMaterialFromIntensity(LIGHT_INTENSITIES[i])));
	}
	_updateLightUniforms();
}
