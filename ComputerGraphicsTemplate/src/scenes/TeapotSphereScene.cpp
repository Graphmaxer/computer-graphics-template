#include <glm/gtc/type_ptr.hpp>
#include "lib/imgui/imgui.h"
#include "scenes/TeapotSphereScene.h"
#include "objects/CheckeredFloor.h"
#include "objects/VBOTeapot.h"
#include "objects/Sphere.h"
#include "objects/Light.h"

TeapotSphereScene::TeapotSphereScene(int width, int height) :
	AScene(width, height, "Teapot and Sphere"),
	_checkeredFloor(std::make_unique<CheckeredFloor>(100, 16)),
	_teapot(std::make_unique<VBOTeapot>(8, glm::mat4())),
	_sphere(std::make_unique<Sphere>(1.0f, 100, 100)),
	_light(std::make_unique<Light>()),
	_lightPosition(10.0f, 10.0f, 0.0f)
{
}

void TeapotSphereScene::_initScene()
{
	_objects.push_back(ObjectWrapper(*_checkeredFloor.get()));
	_objectsInLight.push_back(ObjectWrapper(*_sphere.get(), MAT_RUBY));
	_objectsInLight.push_back(ObjectWrapper(*_teapot.get(), MAT_EMERALD));
	_lights.push_back(LightWrapper(*_light.get()));
}

void TeapotSphereScene::_setupImGui()
{
	if (ImGui::Begin("Light control")) {
		ImGui::SliderFloat3("Light Position", glm::value_ptr(_lightPosition), -10, 10);
	}
	ImGui::End();
}

void TeapotSphereScene::_setupScene()
{
	_lights[0].translate(_lightPosition);
	_objectsInLight[0].translate({ 0, 1, 3.5 });
	_objectsInLight[1].rotate(-90, { 1, 0, 0 });
}
