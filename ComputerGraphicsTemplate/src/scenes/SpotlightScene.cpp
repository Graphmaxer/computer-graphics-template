#include <glm/gtc/type_ptr.hpp>
#include "lib/imgui/imgui.h"
#include "scenes/SpotlightScene.h"
#include "objects/Plane.h"
#include "objects/VBOTeapot.h"
#include "objects/Light.h"

SpotlightScene::SpotlightScene(int width, int height) :
	AScene(width, height, "Spotlight"),
	_plane(std::make_unique<Plane>(100, 16)),
	_teapot(std::make_unique<VBOTeapot>(8, glm::mat4())),
	_light(std::make_unique<Light>()),
	_lightRotation(0)
{
}

void SpotlightScene::_initScene()
{
	_objectsInSpotlight.push_back(ObjectWrapper(*_plane.get(), MAT_PLASTIC_WHITE));
	_objectsInSpotlight.push_back(ObjectWrapper(*_teapot.get(), MAT_EMERALD));
	_lights.push_back(LightWrapper(*_light.get(), MAT_DEFAULT, 30, 30 * 0.7f, 10, { 0, 0, 0 }));
}

void SpotlightScene::_setupImGui()
{
}

void SpotlightScene::_setupScene()
{
	glm::vec3 lightPos = glm::vec3(cos(glm::radians(_lightRotation)) * 10, 15, sin(glm::radians(_lightRotation)) * 10);
	_lights[0].translate(lightPos);
	_objectsInSpotlight[1].rotate(-90, { 1, 0, 0 });

	_lightRotation += 2;
}
