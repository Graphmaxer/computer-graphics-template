#include "GlApp.h"

const char *GlApp::GLSL_VERSION = "#version 450";

GlApp::GlApp(int width, int height) :
	_width(width),
	_height(height),
	_myWindow(nullptr),
	_glfwWindow(nullptr),
	_inputHandler(nullptr)
{
	_initialize();
}

GlApp::~GlApp()
{
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
	glfwDestroyWindow(_glfwWindow);
	glfwTerminate();
	delete _myWindow;
	delete _inputHandler;
}

void GlApp::_initialize()
{
	_initGlfw();
	_initImGui();
	glfwMakeContextCurrent(_glfwWindow);
	_initGlew();
	glfwSwapInterval(1);
	_myWindow = new GlWindow(_width, _height);
	printf("OpenGL %s, GLSL %s\n", glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
	_initInputHandler();
}

void GlApp::_initGlfw()
{
	if (!glfwInit())
		throw std::exception("glfwInit error");
	_glfwWindow = glfwCreateWindow(_width, _height, "OpenGL Framework", NULL, NULL);
	if (!_glfwWindow) {
		glfwTerminate();
		throw std::exception("glfwCreateWindow error");
	}
}

void GlApp::_initImGui()
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGui_ImplGlfw_InitForOpenGL(_glfwWindow, true);
	ImGui_ImplOpenGL3_Init(GLSL_VERSION);
	ImGui::StyleColorsDark();
}

void GlApp::_initGlew()
{
	GLenum err = glewInit();
	if (err != GLEW_OK)
		throw std::exception("glewInit error");
}

void GlApp::_initInputHandler()
{
	_inputHandler = new InputHandler(*_myWindow);
	glfwSetWindowUserPointer(_glfwWindow, _inputHandler);
	glfwSetWindowSizeCallback(_glfwWindow, InputHandler::windowSizeCallback);
	glfwSetMouseButtonCallback(_glfwWindow, InputHandler::mouseButtonCallback);
	glfwSetCursorPosCallback(_glfwWindow, InputHandler::cursePosCallback);
	glfwSetKeyCallback(_glfwWindow, InputHandler::keyCallback);
}

void GlApp::run()
{
	while (!glfwWindowShouldClose(_glfwWindow))
	{
		int display_w, display_h;
		glfwGetFramebufferSize(_glfwWindow, &display_w, &display_h);

		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		_myWindow->draw();

		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glfwSwapBuffers(_glfwWindow);
		glfwPollEvents();
		_inputHandler->handleMouseDragging(static_cast<float>(display_w), static_cast<float>(display_h));
	}
}
