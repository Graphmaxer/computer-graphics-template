#include "lib/imgui/imgui.h"
#include "InputHandler.h"

InputHandler::InputHandler(GlWindow &window) :
	_window(window),
	_lbuttonDown(false),
	_rbuttonDown(false),
	_mbuttonDown(false),
	_zoomInDown(false),
	_zoomOutDown(false),
	_mouseX(0),
	_mouseY(0),
	_lastMouseX(0),
	_lastMouseY(0)
{
}

void InputHandler::windowSizeCallback(GLFWwindow *glfwWindow, int width, int height)
{
	InputHandler::_getInputHandlerFromGlfwWindow(glfwWindow)->handleWindowSize(glfwWindow, width, height);
}

void InputHandler::keyCallback(GLFWwindow *glfwWindow, int key, int scancode, int action, int mods)
{
	InputHandler::_getInputHandlerFromGlfwWindow(glfwWindow)->handleKey(glfwWindow, key, scancode, action, mods);
}

void InputHandler::cursePosCallback(GLFWwindow *glfwWindow, double xpos, double ypos)
{
	InputHandler::_getInputHandlerFromGlfwWindow(glfwWindow)->handleCursePos(glfwWindow, xpos, ypos);
}

void InputHandler::mouseButtonCallback(GLFWwindow *glfwWindow, int button, int action, int mods)
{
	InputHandler::_getInputHandlerFromGlfwWindow(glfwWindow)->handleMouseButton(glfwWindow, button, action, mods);
}

void InputHandler::handleWindowSize(GLFWwindow *glfwWindow, int width, int height)
{
	_window.setSize(width, height);
}

void InputHandler::handleKey(GLFWwindow *glfwWindow, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(glfwWindow, GLFW_TRUE);
	if (key == GLFW_KEY_KP_ADD) {
		if (action == GLFW_PRESS)
			_zoomInDown = true;
		else if (action == GLFW_RELEASE)
			_zoomInDown = false;
	}
	if (key == GLFW_KEY_KP_SUBTRACT) {
		if (action == GLFW_PRESS)
			_zoomOutDown = true;
		else if (action == GLFW_RELEASE)
			_zoomOutDown = false;
	}
}

void InputHandler::handleCursePos(GLFWwindow *glfwWindow, double xpos, double ypos)
{
	_mouseX = xpos;
	_mouseY = ypos;
}

void InputHandler::handleMouseButton(GLFWwindow *window, int button, int action, int mods)
{
	if (action == GLFW_PRESS) {
		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
		_lastMouseX = xpos;
		_lastMouseY = ypos;
	}
	if (button == GLFW_MOUSE_BUTTON_LEFT) {
		if (GLFW_PRESS == action)
			_lbuttonDown = true;
		else if (GLFW_RELEASE == action)
			_lbuttonDown = false;
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT) {
		if (GLFW_PRESS == action)
			_rbuttonDown = true;
		else if (GLFW_RELEASE == action)
			_rbuttonDown = false;
	}
	else if (button == GLFW_MOUSE_BUTTON_MIDDLE) {
		if (GLFW_PRESS == action)
			_mbuttonDown = true;
		else if (GLFW_RELEASE == action)
			_mbuttonDown = false;
	}
}

void InputHandler::handleMouseDragging(float width, float height)
{
	ImGuiIO &io = ImGui::GetIO();
	if (io.WantCaptureMouse)
		return;
	if (_lbuttonDown || _mbuttonDown || _rbuttonDown) {
		float fractionChangeX = static_cast<float>(_mouseX - _lastMouseX) / width;
		float fractionChangeY = static_cast<float>(_lastMouseY - _mouseY) / height;
		if (_lbuttonDown)
			_window.getViewer().rotate(fractionChangeX, fractionChangeY);
		else if (_mbuttonDown)
			_window.getViewer().zoom(fractionChangeY);
		else if (_rbuttonDown)
			_window.getViewer().translate(-fractionChangeX, -fractionChangeY, 1);
	}
	if (_zoomInDown)
		_window.getViewer().zoom(0.01f);
	else if (_zoomOutDown)
		_window.getViewer().zoom(-0.01f);
	_lastMouseX = _mouseX;
	_lastMouseY = _mouseY;
}

InputHandler *InputHandler::_getInputHandlerFromGlfwWindow(GLFWwindow *glfwWindow)
{
	return static_cast<InputHandler *>(glfwGetWindowUserPointer(glfwWindow));
}
