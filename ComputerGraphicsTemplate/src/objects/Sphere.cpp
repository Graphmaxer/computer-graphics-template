#include "objects/Sphere.h"

#include <glm//gtc/constants.hpp>
#include <glm/vec3.hpp>
#include <glm/mat3x3.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

Sphere::Sphere()
{
}

Sphere::~Sphere()
{
}

Sphere::Sphere(float rad, GLuint sl, GLuint st) :
radius(rad), slices(sl), stacks(st)
{
	nVerts = (slices + 1) * (stacks + 1);
	elements = (slices * 2 * (stacks - 1)) * 3;

	// Verts
	float * v = new float[3 * nVerts];
	// Normals
	float * n = new float[3 * nVerts];
	// Tex coords
	float * tex = new float[2 * nVerts];    //we don't use it now
	// Index
	unsigned int * el = new unsigned int[elements];  //index

	// Generate the vertex data
	generateVerts(v, n, tex, el);

	//create vao, vbo and ibo here... (We didn't use std::vector here...)
	_setup(v, n, el, 3 * nVerts, elements);

	delete[] v;
	delete[] n;
	delete[] el;
	delete[] tex;
}

void Sphere::draw()
{
	glBindVertexArray(VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	int size;
	glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
	glDrawElements(GL_TRIANGLES, size / sizeof(unsigned int), GL_UNSIGNED_INT, 0);
}

void Sphere::generateVerts(float * verts, float * norms, float * tex,
	unsigned int * el)
{
	// Generate positions and normals
	GLfloat theta, phi;
	GLfloat thetaFac = glm::two_pi<float>() / slices;
	GLfloat phiFac = glm::pi<float>() / stacks;
	GLfloat nx, ny, nz, s, t;
	GLuint idx = 0, tIdx = 0;
	for (GLuint i = 0; i <= slices; i++) {
		theta = i * thetaFac;
		s = (GLfloat)i / slices;
		for (GLuint j = 0; j <= stacks; j++) {
			phi = j * phiFac;
			t = (GLfloat)j / stacks;
			nx = sinf(phi) * cosf(theta);
			ny = sinf(phi) * sinf(theta);
			nz = cosf(phi);
			verts[idx] = radius * nx; verts[idx + 1] = radius * ny; verts[idx + 2] = radius * nz;
			norms[idx] = nx; norms[idx + 1] = ny; norms[idx + 2] = nz;
			idx += 3;

			tex[tIdx] = s;
			tex[tIdx + 1] = t;
			tIdx += 2;
		}
	}

	// Generate the element list
	idx = 0;
	for (GLuint i = 0; i < slices; i++) {
		GLuint stackStart = i * (stacks + 1);
		GLuint nextStackStart = (i + 1) * (stacks + 1);
		for (GLuint j = 0; j < stacks; j++) {
			if (j == 0) {
				el[idx] = stackStart;
				el[idx + 1] = stackStart + 1;
				el[idx + 2] = nextStackStart + 1;
				idx += 3;
			}
			else if (j == stacks - 1) {
				el[idx] = stackStart + j;
				el[idx + 1] = stackStart + j + 1;
				el[idx + 2] = nextStackStart + j;
				idx += 3;
			}
			else {
				el[idx] = stackStart + j;
				el[idx + 1] = stackStart + j + 1;
				el[idx + 2] = nextStackStart + j + 1;
				el[idx + 3] = nextStackStart + j;
				el[idx + 4] = stackStart + j;
				el[idx + 5] = nextStackStart + j + 1;
				idx += 6;
			}
		}
	}
}

void Sphere::_setup(float *vertices, float *normals, unsigned int *elements, int nbVertices, int nbElements)
{
	// create vao
	glGenVertexArrays(1, &VAO); // create 1 vao and link it to the vao handler
	glBindVertexArray(VAO); // activate the vao

	// create vbo #1: positions
	glGenBuffers(1, &VBO_position); // create 1 vbo and link it to the vbo handler
	glBindBuffer(GL_ARRAY_BUFFER, VBO_position); // activate the vbo and specify its type
	glBufferData(GL_ARRAY_BUFFER, nbVertices * sizeof(float), vertices, GL_STATIC_DRAW); // fill the buffer, the data is static
	glVertexAttribPointer(
		0, // attr number = 0
		3, // number of dimensions
		GL_FLOAT, // data type
		GL_FALSE, // is normalized
		0, // byte offset between vertex
		0 // address offset
	);
	glEnableVertexAttribArray(0);  // attr number = 0

	// create vbo #2: normals
	glGenBuffers(1, &VBO_normal); // create 1 vbo and link it to the vbo handler
	glBindBuffer(GL_ARRAY_BUFFER, VBO_normal); // activate the vbo and specify its type
	glBufferData(GL_ARRAY_BUFFER, nbVertices * sizeof(float), normals, GL_STATIC_DRAW); // fill the buffer, the data is static
	glVertexAttribPointer(
		1, // attr number = 1
		3, // number of dimensions
		GL_FLOAT, // data type
		GL_FALSE, // is normalized
		0, // byte offset between vertex
		0 // address offset
	);
	glEnableVertexAttribArray(1);  // attr number = 1

	// create vbo #3: indices
	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, nbElements * sizeof(unsigned int), elements, GL_STATIC_DRAW);

	glBindVertexArray(0);  //unbounding the VAO
}

int Sphere::getVertexArrayHandle()
{
	return this->VAO;
}
