#include "objects/LineSegment.h"

LineSegment::LineSegment()
{
	_setup();
}

void LineSegment::_setup()
{
	GLfloat lineVertices[] = {
		0, -1, 0,
		0, 1, 0,
	};

	GLfloat lineColors[] = {
		1, 0, 0,
		1, 0, 0,
	};

	// create vao
	glGenVertexArrays(1, &_vaoHandle); // create 1 vao and link it to the vao handler
	glBindVertexArray(_vaoHandle); // activate the vao

	// create vbo #1: positions
	glGenBuffers(1, &_vboLineVertices); // create 1 vbo and link it to the vbo handler
	glBindBuffer(GL_ARRAY_BUFFER, _vboLineVertices); // activate the vbo and specify its type
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineVertices), &lineVertices, GL_STATIC_DRAW); // fill the buffer, the data is static
	glVertexAttribPointer(
		0, // attr number = 0
		3, // number of dimensions
		GL_FLOAT, // data type
		GL_FALSE, // is normalized
		0, // byte offset between vertex
		0 // address offset
	);
	glEnableVertexAttribArray(0);  // attr number = 0

	// create vbo #2: colors
	glGenBuffers(1, &_vboLineColors); // create 1 vbo and link it to the vbo handler
	glBindBuffer(GL_ARRAY_BUFFER, _vboLineColors); // activate the vbo and specify its type
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineColors), &lineColors, GL_STATIC_DRAW); // fill the buffer, the data is static
	glVertexAttribPointer(
		1, // attr number = 1
		3, // number of dimensions
		GL_FLOAT, // data type
		GL_FALSE, // is normalized
		0, // byte offset between vertex
		0 // address offset
	);
	glEnableVertexAttribArray(1);  // attr number = 1

	glBindVertexArray(0);  //unbounding the VAO
}

void LineSegment::draw()
{
	glBindVertexArray(_vaoHandle);
	glLineWidth(2.0);
	glDrawArrays(GL_LINES, 0, 2);
}
