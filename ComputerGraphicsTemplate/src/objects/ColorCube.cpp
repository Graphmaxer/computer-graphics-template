#include "objects/ColorCube.h"

ColorCube::ColorCube()
{
	_setup();
}

void ColorCube::_setup()
{
	GLfloat cubeVertices[] = {
		// front
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,
		1.0, 1.0, 1.0,
		-1.0, 1.0, 1.0,
		// back
		-1.0, -1.0, -1.0,
		1.0, -1.0, -1.0,
		1.0, 1.0, -1.0,
		-1.0, 1.0, -1.0,
	};

	GLfloat cubeColors[] = {
		// front colors
		1.0, 0.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 0.0, 1.0,
		1.0, 1.0, 1.0,
		// back colors
		1.0, 0.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 0.0, 1.0,
		1.0, 1.0, 1.0,
	};

	GLushort cubeElements[] = {
		0, 1, 2,		2, 3, 0,		1, 5, 6,
		6, 2, 1,		7, 6, 5,		5, 4, 7,
		4, 0, 3,		3, 7, 4,		4, 5, 1,
		1, 0, 4,		3, 2, 6,		6, 7, 3,
	};

	// create vao
	glGenVertexArrays(1, &_vaoHandle); // create 1 vao and link it to the vao handler
	glBindVertexArray(_vaoHandle); // activate the vao

	// create vbo #1: positions
	glGenBuffers(1, &_vboCubeVertices); // create 1 vbo and link it to the vbo handler
	glBindBuffer(GL_ARRAY_BUFFER, _vboCubeVertices); // activate the vbo and specify its type
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), &cubeVertices, GL_STATIC_DRAW); // fill the buffer, the data is static
	glVertexAttribPointer(
		0, // attr number = 0
		3, // number of dimensions
		GL_FLOAT, // data type
		GL_FALSE, // is normalized
		0, // byte offset between vertex
		0 // address offset
	);
	glEnableVertexAttribArray(0);  // attr number = 0

	// create vbo #2: colors
	glGenBuffers(1, &_vboCubeColors); // create 1 vbo and link it to the vbo handler
	glBindBuffer(GL_ARRAY_BUFFER, _vboCubeColors); // activate the vbo and specify its type
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeColors), &cubeColors, GL_STATIC_DRAW); // fill the buffer, the data is static
	glVertexAttribPointer(
		1, // attr number = 1
		3, // number of dimensions
		GL_FLOAT, // data type
		GL_FALSE, // is normalized
		0, // byte offset between vertex
		0 // address offset
	);
	glEnableVertexAttribArray(1);  // attr number = 1

	// create vbo #3: indices
	glGenBuffers(1, &_iboCubeElements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _iboCubeElements);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeElements), cubeElements, GL_STATIC_DRAW);

	glBindVertexArray(0);  //unbounding the VAO
}

void ColorCube::draw()
{
	glBindVertexArray(_vaoHandle);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _iboCubeElements);
	int size;
	glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
	glDrawElements(GL_TRIANGLES, size / sizeof(GLushort), GL_UNSIGNED_SHORT, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
