#include <vector>
#include <iostream>
#include "objects/WireCube.h"

WireCube::WireCube()
{
	_setup();
}

void WireCube::_setup()
{
	std::vector<GLfloat> cubeVertices = {
		// front
		1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,

		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,

		-1.0f, -1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,

		// right
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,

		1.0f, 1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, 1.0f,

		// back
		-1.0f, 1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,

		// left
		-1.0f, -1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,

		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,

		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,

		// top
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,

		// bottom
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
	};
	std::vector<GLfloat> cubeColors;

	for (auto cubeElement : cubeVertices) {
		cubeColors.push_back(0);
	}

	// create vao
	glGenVertexArrays(1, &_vaoHandle); // create 1 vao and link it to the vao handler
	glBindVertexArray(_vaoHandle); // activate the vao

	// create vbo #1: positions
	glGenBuffers(1, &_vboCubeVertices); // create 1 vbo and link it to the vbo handler
	glBindBuffer(GL_ARRAY_BUFFER, _vboCubeVertices); // activate the vbo and specify its type
	glBufferData(GL_ARRAY_BUFFER, cubeVertices.size() * sizeof(GLfloat), cubeVertices.data(), GL_STATIC_DRAW); // fill the buffer, the data is static
	glVertexAttribPointer(
		0, // attr number = 0
		3, // number of dimensions
		GL_FLOAT, // data type
		GL_FALSE, // is normalized
		0, // byte offset between vertex
		0 // address offset
	);
	glEnableVertexAttribArray(0);  // attr number = 0

	// create vbo #2: colors
	glGenBuffers(1, &_vboCubeColors); // create 1 vbo and link it to the vbo handler
	glBindBuffer(GL_ARRAY_BUFFER, _vboCubeColors); // activate the vbo and specify its type
	glBufferData(GL_ARRAY_BUFFER, cubeColors.size() * sizeof(GLfloat), cubeColors.data(), GL_STATIC_DRAW); // fill the buffer, the data is static
	glVertexAttribPointer(
		1, // attr number = 1
		3, // number of dimensions
		GL_FLOAT, // data type
		GL_FALSE, // is normalized
		0, // byte offset between vertex
		0 // address offset
	);
	glEnableVertexAttribArray(1);  // attr number = 1

	glBindVertexArray(0);  //unbounding the VAO
}

void WireCube::draw()
{
	glBindVertexArray(_vaoHandle);
	glLineWidth(2.0);
	glDrawArrays(GL_LINES, 0, 36);
}
