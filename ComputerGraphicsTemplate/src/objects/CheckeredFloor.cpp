#include <vector>
#include <glm/glm.hpp>
#include "objects/CheckeredFloor.h"

CheckeredFloor::CheckeredFloor(int size, int nSquares) : _size(size), _nSquares(nSquares)
{
	_setup();
}

void CheckeredFloor::_setup()
{
	glm::vec3 floorColor1 = { .7f, .7f, .7f };
	glm::vec3 floorColor2 = { .3f, .3f, .3f };
	std::vector<glm::vec3> tileVertices;
	std::vector<glm::vec3> tileColors;
	float maxX = _size / 2.0f;
	float maxY = _size / 2.0f;
	float minX = -_size / 2.0f;
	float minY = -_size / 2.0f;
	float xd = (maxX - minX) / ((float)_nSquares);
	float yd = (maxY - minY) / ((float)_nSquares);
	float xp;
	float yp;
	int x;
	int y;

	for (x = 0, xp = minX; x < _nSquares; x++, xp += xd) {
		for (y = 0, yp = minY; y < _nSquares; y++, yp += yd) {
			glm::vec3 color = floorColor1;
			if ((x + y) % 2 == 0)
				color = floorColor2;
			tileVertices.push_back(glm::vec3(xp, 0, yp));
			tileColors.push_back(color);
			tileVertices.push_back(glm::vec3(xp, 0, yp + yd));
			tileColors.push_back(color);
			tileVertices.push_back(glm::vec3(xp + xd, 0, yp));
			tileColors.push_back(color);
			tileVertices.push_back(glm::vec3(xp + xd, 0, yp + yd));
			tileColors.push_back(color);
			tileVertices.push_back(glm::vec3(xp + xd, 0, yp));
			tileColors.push_back(color);
			tileVertices.push_back(glm::vec3(xp, 0, yp + yd));
			tileColors.push_back(color);
		}
	}

	// create vao
	glGenVertexArrays(1, &_vaoHandle); // create 1 vao and link it to the vao handler
	glBindVertexArray(_vaoHandle); // activate the vao

	// create vbo #1: positions
	glGenBuffers(1, &_vboTileVertices);
	glBindBuffer(GL_ARRAY_BUFFER, _vboTileVertices);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * tileVertices.size(), tileVertices.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(
		0, // attr number = 0
		3, // number of dimensions
		GL_FLOAT, // data type
		GL_FALSE, // is normalized
		0, // byte offset between vertex
		0 // address offset
	);
	glEnableVertexAttribArray(0);  // attr number = 0

	// create vbo #2: colors
	glGenBuffers(1, &_vboTileColors); // create 1 vbo and link it to the vbo handler
	glBindBuffer(GL_ARRAY_BUFFER, _vboTileColors); // activate the vbo and specify its type
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * tileColors.size(), tileColors.data(), GL_STATIC_DRAW); // fill the buffer, the data is static
	glVertexAttribPointer(
		1, // attr number = 1
		3, // number of dimensions
		GL_FLOAT, // data type
		GL_FALSE, // is normalized
		0, // byte offset between vertex
		0 // address offset
	);
	glEnableVertexAttribArray(1);  // attr number = 1
}

void CheckeredFloor::draw()
{
	glBindVertexArray(_vaoHandle);
	glDrawArrays(GL_TRIANGLES, 0, _nSquares * _size * 2);
}
