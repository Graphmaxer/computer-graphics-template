#include "objects/Light.h"

Light::Light()
{
	_setup();
}

void Light::_setup()
{
	float pos[3] = { 0.0f, 0.0f, 0.0f };
	float color[3] = { 1.0f, 0.0f, 0.0f };

	// create vao
	glGenVertexArrays(1, &_vaoHandle); // create 1 vao and link it to the vao handler
	glBindVertexArray(_vaoHandle); // activate the vao

	// create vbo #1: positions
	glGenBuffers(1, &_vboLightVertices); // create 1 vbo and link it to the vbo handler
	glBindBuffer(GL_ARRAY_BUFFER, _vboLightVertices); // activate the vbo and specify its type
	glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float), pos, GL_STATIC_DRAW); // fill the buffer, the data is static
	glVertexAttribPointer(
		0, // attr number = 0
		3, // number of dimensions
		GL_FLOAT, // data type
		GL_FALSE, // is normalized
		0, // byte offset between vertex
		0 // address offset
	);
	glEnableVertexAttribArray(0);  // attr number = 0

	// create vbo #2: colors
	glGenBuffers(1, &_vboLightColors); // create 1 vbo and link it to the vbo handler
	glBindBuffer(GL_ARRAY_BUFFER, _vboLightColors); // activate the vbo and specify its type
	glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float), color, GL_STATIC_DRAW); // fill the buffer, the data is static
	glVertexAttribPointer(
		1, // attr number = 1
		3, // number of dimensions
		GL_FLOAT, // data type
		GL_FALSE, // is normalized
		0, // byte offset between vertex
		0 // address offset
	);
	glEnableVertexAttribArray(1);  // attr number = 1

	glBindVertexArray(0);  //unbounding the VAO
}

void Light::draw()
{
	glBindVertexArray(_vaoHandle);
	glDrawArrays(GL_POINTS, 0, 1);
}

Material Light::createMaterialFromIntensity(const glm::vec3 &intensity)
{
	return {
		intensity,
		intensity,
		intensity,
		0.0f
	};
}

glm::vec3 Light::getIntensityFromMaterial(const Material &material)
{
	return material.Ka;
}
