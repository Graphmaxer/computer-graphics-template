#include "ObjectWrapper.h"

ObjectWrapper::ObjectWrapper(IRaw3DObject &rawObject, const Material &material) :
	_model(),
	_rawObject(rawObject),
	_isActive(true),
	_material(material)
{
}

void ObjectWrapper::draw()
{
	if (_isActive)
		_rawObject.draw();
}

void ObjectWrapper::translate(const glm::vec3 &position)
{
	_model.glTranslate(position.x, position.y, position.z);
}

void ObjectWrapper::rotate(const float &degree, const glm::vec3 &axes)
{
	_model.glRotate(degree, axes.x, axes.y, axes.z);
}

void ObjectWrapper::scale(const glm::vec3 &scale)
{
	_model.glScale(scale.x, scale.y, scale.z);
}

void ObjectWrapper::reset()
{
	_model = Model();
}

void ObjectWrapper::setActive(bool isActive)
{
	_isActive = isActive;
}

glm::mat4 ObjectWrapper::getModelMatrix()
{
	return _model.getMatrix();
}

glm::vec4 ObjectWrapper::getPosition()
{
	glm::mat4 model = _model.getMatrix();

	return model[3];
}

const Material &ObjectWrapper::getMaterial() const
{
	return _material;
}

void ObjectWrapper::setMaterial(const Material &material)
{
	_material = material;
}
