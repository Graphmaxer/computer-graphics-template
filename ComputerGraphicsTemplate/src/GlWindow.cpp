#include "lib/imgui/imgui.h"
#include "GlWindow.h"
#include "scenes/ThreeCubesScene.h"
#include "scenes/CraneScene.h"
#include "scenes/TeapotSphereScene.h"
#include "scenes/DragonScene.h"
#include "scenes/SpotlightScene.h"

GlWindow::GlWindow(int width, int height) :
	_width(width),
	_height(height),
	_clearColor(0.45f, 0.55f, 0.60f, 1.00f),
	_scenes(),
	_currentSceneIdx(0)
{
	_scenes.push_back(std::make_unique<SpotlightScene>(width, height));
	_scenes.push_back(std::make_unique<DragonScene>(width, height));
	_scenes.push_back(std::make_unique<TeapotSphereScene>(width, height));
	_scenes.push_back(std::make_unique<ThreeCubesScene>(width, height));
	_scenes.push_back(std::make_unique<CraneScene>(width, height));
	for (auto &scene : _scenes)
		scene->init();
}

void GlWindow::setSize(int width, int height)
{
	_width = width;
	_height = height;
	_scenes[_currentSceneIdx]->setSize(width, height);
}

Viewer &GlWindow::getViewer()
{
	return _scenes[_currentSceneIdx]->getViewer();
}

void GlWindow::_drawSceneManagerUi()
{
	if (ImGui::Begin("Scene Manager")) {
		for (size_t i = 0; i < _scenes.size(); ++i) {
			if (ImGui::Button(_scenes[i]->getName().c_str())) {
				_currentSceneIdx = i;
			}
		}
		ImGui::ColorEdit4("Clear Color", reinterpret_cast<float *>(&_clearColor));
	}
	ImGui::End();
}

void GlWindow::draw()
{
	glClearColor(_clearColor.x, _clearColor.y, _clearColor.z, _clearColor.w);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glEnable(GL_PROGRAM_POINT_SIZE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glCullFace(GL_BACK);
	glViewport(0, 0, _width, _height);

	_drawSceneManagerUi();
	_scenes[_currentSceneIdx]->render();
}
