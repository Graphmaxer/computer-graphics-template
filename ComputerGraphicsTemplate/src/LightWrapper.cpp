#pragma once

#include "LightWrapper.h"

LightWrapper::LightWrapper(
	IRaw3DObject &rawObject,
	const Material &material,
	float cutoff,
	float innerCutoff,
	float exponent,
	const glm::vec3 &lookAtPos
) :
	ObjectWrapper(rawObject, material),
	_cutoff(cutoff),
	_innerCutoff(innerCutoff),
	_exponent(exponent),
	_lookAtPos(glm::vec4(lookAtPos, 1))
{
}

float LightWrapper::getCutoff() const
{
	return _cutoff;
}

float LightWrapper::getInnerCutoff() const
{
	return _innerCutoff;
}

float LightWrapper::getExponent() const
{
	return _exponent;
}

glm::vec4 LightWrapper::getLookAtPos() const
{
	return _lookAtPos;
}
