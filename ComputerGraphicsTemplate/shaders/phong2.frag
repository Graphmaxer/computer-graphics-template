#version 450

#define MAX_LIGHTS_NB 32

// Structs
struct LightInfo {
	vec4 position;
	vec3 intensity;
};

// Inputs
in vec4 f_position;
in vec3 f_normal;

// Outputs
out vec4 FragColor;

// Uniforms
uniform LightInfo u_lights[MAX_LIGHTS_NB];
uniform int u_nbLights;

uniform vec3 u_Ka;
uniform vec3 u_Kd;
uniform vec3 u_Ks;
uniform float u_shininess;

void main()
{
	vec3 finalColor = vec3(0, 0, 0);
	vec3 N = f_normal;
	vec3 V = normalize(-f_position.xyz);
	vec3 ambient = u_Ka * u_lights[0].intensity;

	for (int i = 0; i < u_nbLights; i++) {
		vec3 L = normalize((u_lights[i].position - f_position).xyz);
		vec3 R = normalize(reflect(-L, N));
		vec3 diffuse = u_Kd * u_lights[i].intensity * max(dot(N, L), 0.0);
		vec3 specular = u_Ks * u_lights[i].intensity * pow(max(dot(R, V), 0.0), u_shininess);
		finalColor = finalColor + diffuse + specular;
	}
	finalColor = finalColor + ambient;
	FragColor = vec4(finalColor, 1.0f);
}
