#version 450

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_color;

uniform mat4 u_mvp;

out vec3 f_color;

void main(void)
{
	gl_Position = u_mvp * vec4(v_position, 1.0f);
	gl_PointSize = 10.0f;
	f_color = v_color;
}
