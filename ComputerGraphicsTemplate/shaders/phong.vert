#version 450

// In
layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;

// Uniform
uniform mat4 u_mvp;
uniform mat4 u_modelViewMatrix; // view * model
uniform mat3 u_normalMatrix; // Transformation matrix for normal

// Out
out vec3 f_color;

// Global light position
const vec4 lightPos = vec4(10, 10, 0, 1);

// Object colors
const vec3 Ka = vec3(0.2, 0.2, 0);
const vec3 Kd = vec3(1, 1, 0);
const vec3 Ks = vec3(0.2, 0.2, 0);

// Light colors
const vec3 Ia = vec3(1, 1, 1);
const vec3 Id = vec3(1, 1, 1);
const vec3 Is = vec3(1, 1, 1);

const float phongExponent = 20.0;

void main()
{
  vec4 pos = u_modelViewMatrix * vec4(v_position, 1.0);

  vec3 N = normalize(u_normalMatrix * v_normal);
  vec3 L = normalize((lightPos - pos).xyz);
  vec3 V = normalize(-pos.xyz);
  vec3 R = normalize(reflect(-L, N));

  vec3 ambient = Ka * Ia;
  vec3 diffuse = Kd * Id * max(dot(N, L), 0.0);
  vec3 specular = Ks * Is * pow(max(dot(R, V), 0.0), phongExponent);

  f_color = ambient + diffuse + specular;

  gl_Position = u_mvp * vec4(v_position, 1.0);
}
