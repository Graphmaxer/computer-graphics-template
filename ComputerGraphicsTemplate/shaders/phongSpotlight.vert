#version 450

// In
layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;

// Uniform
uniform mat4 u_mvp;
uniform mat4 u_modelViewMatrix; // view * model
uniform mat3 u_normalMatrix; // Transformation matrix for normal

// Out
out vec4 f_position;
out vec3 f_normal;

void main()
{
	f_position = u_modelViewMatrix * vec4(v_position, 1.0);
	f_normal = normalize(u_normalMatrix * v_normal);
	gl_Position = u_mvp * vec4(v_position, 1.0);
}
