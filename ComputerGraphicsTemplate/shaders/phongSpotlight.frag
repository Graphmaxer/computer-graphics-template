#version 450

#define MAX_LIGHTS_NB 32

// Constants
const float attConst = 1.0f;
const float attLinear = 0.01f;
const float attQuadratic = 0.001f;

// Structs
struct LightInfo {
	vec4 position;
	vec3 intensity;
	float cutoff;
	float innerCutoff;
	float exponent;
	vec3 direction;
};

// Inputs
in vec4 f_position;
in vec3 f_normal;

// Outputs
out vec4 FragColor;

// Uniforms
uniform LightInfo u_lights[MAX_LIGHTS_NB];
uniform int u_nbLights;

uniform vec3 u_Ka;
uniform vec3 u_Kd;
uniform vec3 u_Ks;
uniform float u_shininess;

float calcAttenuation(LightInfo light, vec4 pos, vec3 L)
{
	float dist = distance(light.position, pos);
	float attenuation = 1.0f / (attConst + (attLinear * dist) + (attQuadratic * pow(dist, 2)));
	float angle = acos(dot(normalize(light.direction), -L));
	float spotlightAttenuation = 1.0f;
	if (angle >= light.innerCutoff) {
		float spot = smoothstep(cos(light.cutoff), cos(light.innerCutoff), cos(angle));
		spotlightAttenuation = pow(spot, light.exponent);
	}
	return attenuation * spotlightAttenuation;
}

void main()
{
	vec3 finalColor = vec3(0, 0, 0);
	vec3 N = f_normal;
	vec3 V = normalize(-f_position.xyz);
	float ambiantAttenuation = calcAttenuation(u_lights[0], f_position, normalize((u_lights[0].position - f_position).xyz));
	vec3 ambient = u_Ka * u_lights[0].intensity * ambiantAttenuation;

	for (int i = 0; i < u_nbLights; i++) {
		vec3 L = normalize((u_lights[i].position - f_position).xyz);
		vec3 R = normalize(reflect(-L, N));
		float attenuation = calcAttenuation(u_lights[i], f_position, L);
		vec3 diffuse = u_Kd * u_lights[i].intensity * max(dot(N, L), 0.0) * attenuation;
		vec3 specular = u_Ks * u_lights[i].intensity * pow(max(dot(R, V), 0.0), u_shininess) * attenuation;
		finalColor = finalColor + diffuse + specular;
	}
	finalColor = finalColor + ambient;
	FragColor = vec4(finalColor, 1.0f);
}
